## Commandments وصايا

وَعَنْهُ أَنَّ رَجُلاً قَالَ: { يَا رَسُولَ اَللَّهِ! أَوْصِنِي.‏ فَقَالَ: لَا تَغْضَبْ, فَرَدَّدَ مِرَارًا.‏ قَالَ: لَا تَغْضَبْ } أَخْرَجَهُ اَلْبُخَارِيُّ.‏ (1954)‏ .‏

صحيح.‏ رواه البخاري (6116)‏.‏

Abu Hurairah (RAA) narrated, ‘A man said, “O Messenger of Allah, advise me.” The Messenger of Allah (ﷺ) said:
“Do not get angry.” The man repeated that several times and he replied, “Do not get angry.” Related by Al-Bukhari.

English reference	 : Book 16, Hadith 1535
Arabic reference	 : Book 16, Hadith 1492

وَحَدَّثَنِي عَنْ مَالِكٍ، عَنِ ابْنِ شِهَابٍ، عَنْ عَلِيِّ بْنِ حُسَيْنِ بْنِ عَلِيِّ بْنِ أَبِي طَالِبٍ، أَنَّ رَسُولَ اللَّهِ صلى الله عليه وسلم قَالَ ‏ "‏ مِنْ حُسْنِ إِسْلاَمِ الْمَرْءِ تَرْكُهُ مَا لاَ يَعْنِيهِ ‏"‏ ‏.‏

 موطأالإمام مالك كتاب حسن الخلق

Yahya related to me from Malik from Ibn Shihab from Ali ibn Husayn ibn Ali ibn Abi Talib that the Messenger of Allah, may Allah bless him and grant him peace, said, "Part of the excellence of a man's Islam is that he leaves what does not concern him."

Muwatta Malik Good Character
USC-MSA web (English) reference	 : Book 47, Hadith 3
Arabic reference	 : Book 47, Hadith 1638

عن أنس رضي الله عنه عن النبي صلى الله عليه وسلم قال‏:‏ “لا يؤمن أحدكم حتى يحب لأخيه ما يحب لنفسه‏"‏ ‏(‏‏(‏متفق عليه‏)‏‏)‏ ‏

Anas (May Allah be pleased with him) reported:
The Prophet (ﷺ) said, "No one of you becomes a true believer until he likes for his brother what he likes for himself".

[Al-Bukhari and Muslim].
Arabic/English book reference	 : Book 1, Hadith 183

جعفر الصادق كان يعزي الحسن البصري يقول له:يا حسن أطع من أحسن إليك فإن لم تطعه فلا تعص له أمراً، وإن لم تطعه وعصيت أمره فلا تأكل من رزقه، وإن لم تطعه وعصيت أمره وأكلت من رزقه وسكنت داره فأعد له جواباً وليكن صواباً
