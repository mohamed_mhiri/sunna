## Abu Dharr May Allah be pleased by himself

حَدَّثَنَا الْعَبَّاسُ الْعَنْبَرِيُّ، حَدَّثَنَا النَّضْرُ بْنُ مُحَمَّدٍ، حَدَّثَنَا عِكْرِمَةُ بْنُ عَمَّارٍ، حَدَّثَنَا أَبُو زُمَيْلٍ، هُوَ سِمَاكُ بْنُ الْوَلِيدِ الْحَنَفِيُّ عَنْ مَالِكِ بْنِ مَرْثَدٍ، عَنْ أَبِيهِ، عَنْ أَبِي ذَرٍّ، قَالَ قَالَ رَسُولُ اللَّهِ صلى الله عليه وسلم ‏"‏ مَا أَظَلَّتِ الْخَضْرَاءُ وَلاَ أَقَلَّتِ الْغَبْرَاءُ مِنْ ذِي لَهْجَةٍ أَصْدَقَ وَلاَ أَوْفَى مِنْ أَبِي ذَرٍّ شِبْهِ عِيسَى ابْنِ مَرْيَمَ عَلَيْهِ السَّلاَمُ ‏"‏ ‏.‏ فَقَالَ عُمَرُ بْنُ الْخَطَّابِ كَالْحَاسِدِ يَا رَسُولَ اللَّهِ أَفَنَعْرِفُ ذَلِكَ لَهُ قَالَ ‏"‏ نَعَمْ فَاعْرِفُوهُ لَهُ ‏"‏ ‏.‏ قَالَ هَذَا حَدِيثٌ حَسَنٌ غَرِيبٌ مِنْ هَذَا الْوَجْهِ ‏.‏ وَقَدْ رَوَى بَعْضُهُمْ هَذَا الْحَدِيثَ فَقَالَ ‏"‏ أَبُو ذَرٍّ يَمْشِي فِي الأَرْضِ بِزُهْدِ عِيسَى ابْنِ مَرْيَمَ عَلَيْهِ السَّلاَمُ ‏"‏ ‏.‏


Narrated Abu Dharr:
that the Messenger of Allah (ﷺ) said: "There is no one more truthful in speech, nor in fulfilling of promises, that sky has covered and the earth has carried, than Abu Dharr, the likeness of 'Eisa bin Mariam." So 'Umar bin Al-Khattab said, as if out of envy: "So do you acknowledge that for him, O Messenger of Allah?" He said: "Yes, so acknowledge it."

Grade	: Hasan (Darussalam)
English reference	 : Vol. 1, Book 46, Hadith 3802
Arabic reference	 : Book 49, Hadith 4172
