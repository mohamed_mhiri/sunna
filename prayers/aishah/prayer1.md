## prayer 1
وَعَنْ عَائِشَةَ ‏-رَضِيَ اَللَّهُ عَنْهَا‏- أَنَّ اَلنَّبِيَّ ‏- صلى الله عليه وسلم ‏-عَلَّمَهَا هَذَا اَلدُّعَاءَ: { اَللَّهُمَّ إِنِّي أَسْأَلُكَ مِنَ الْخَيْرِ كُلِّهِ, عَاجِلِهِ وَآجِلِهِ, مَا عَلِمْتُ مِنْهُ وَمَا لَمْ أَعْلَمْ, وَأَعُوذُ بِكَ مِنَ الشَّرِّ كُلِّهِ, عَاجِلِهِ وَآجِلِهِ, مَا عَلِمْتُ مِنْهُ وَمَا لَمْ أَعْلَمْ, اَللَّهُمَّ إِنِّي أَسْأَلُكَ مِنْ خَيْرِ مَا سَأَلَكَ عَبْدُكَ وَنَبِيُّكَ, وَأَعُوذُ بِكَ مِنْ شَرِّ مَا عَاذَ بِهِ عَبْدُكَ وَنَبِيُّكَ, اَللَّهُمَّ إِنِّي أَسْأَلُكَ اَلْجَنَّةَ, وَمَا قَرَّبَ إِلَيْهَا مِنْ قَوْلٍ أَوْ عَمَلٍ, وَأَعُوذُ بِكَ مِنَ النَّارِ, وَمَا قَرَّبَ مِنْهَا مِنْ قَوْلٍ أَوْ عَمَلٍ, وَأَسْأَلُكَ أَنْ تَجْعَلَ كُلَّ قَضَاءٍ قَضَيْتَهُ لِي خَيْرًا } أَخْرَجَهُ اِبْنُ مَاجَهْ, وَصَحَّحَهُ اِبْنُ حِبَّانَ, وَالْحَاكِمُ

’A’ishah (RAA) narrated, ‘Allah’s Messenger (ﷺ) taught her this supplication, “O Allah! I ask you of all good of what I have done and what I have not done in this world and in the Hereafter. I seek refuge in you from the evil of what I have done and what I have not done in this world and in the Hereafter. O Allah! I ask of you all good that your servant and Prophet Muhammad (ﷺ) used to ask of you. I seek refuge in you from all evil that your servant and Prophet Muhammad used to seek refuge in you from. O Allah! I ask you for Paradise and what brings me nearer to it of deeds and sayings. I seek refuge in You from Hell-Fire and what brings me near to it of deeds and sayings. I ask you for the good consequences of Your Decree.” Related by Ibn Majah Ibn Hibban and Al-Hakim graded it as Sahih

صحيح.‏ رواه ابن ماجه (3846)‏، وابن حبان (869)‏، والحاكم (1 / 512‏- 522)‏ وفي سند ابن حبان سقط.‏

English reference	 : Book 16, Hadith 1610
Arabic reference	 : Book 16, Hadith 1567
