## remembrance 1

وعن أبي موسى الأشعري رضي الله عنه عن النبي صلى الله عليه وسلم، قال‏:‏ ‏"‏مثل الذي يذكر ربه والذي لا يذكره، مثل الحي والميت‏"‏ ‏(‏‏(‏رواه البخاري‏)‏‏)‏‏.‏ 
ورواه مسلم فقال‏:‏ ‏"‏مثل البيت الذي يذكر الله فيه، والبيت الذي لا يذكر الله فيه، مثل الحي والميت‏"‏‏.‏

Abu Musa Al-Ash'ari (May Allah be pleased with him) reported:
The Prophet (ﷺ) said, "The similitude of one who remembers his Rubb and one who does not remember Him, is like that of the living and the dead."

[Al-Bukhari and Muslim].


Sunnah.com reference	 : Book 16, Hadith 27
Arabic/English book reference	 : Book 16, Hadith 1434
